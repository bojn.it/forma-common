declare module 'class-validator/cjs/decorator/decorators.js' {
  export const IS_DEFINED = 'isDefined'
  export * from 'class-validator/types/decorator/decorators.js'
}

declare module 'class-validator/cjs/metadata/ValidationMetadata.js' {
  export * from 'class-validator/types/metadata/ValidationMetadata.js'
}

declare module 'class-validator/cjs/metadata/ValidationMetadataArgs.js' {
  export * from 'class-validator/types/metadata/ValidationMetadataArgs.js'
}
