import { getMetadataStorage as getClassValidatorMetadataStorage } from 'class-validator'
import type { ValidationArguments as ClassValidatorValidationArguments } from 'class-validator'
import { ValidationTypes as ClassValidatorValidationTypes } from 'class-validator'
import type { ValidatorConstraintInterface as ClassValidatorConstraintInterface } from 'class-validator'
import * as classValidators from 'class-validator/cjs/decorator/decorators.js'
import 'reflect-metadata'
import type { Newable } from 'ts-essentials'
import type { Primitive } from 'ts-essentials'
import type { ReadonlyArrayOrSingle } from 'ts-essentials'

declare global {
  var formaMetadataStorage: MetadataStorage | undefined
}

export type FormaValue =
  | Exclude<Primitive, symbol | undefined>
  | { [key: string]: FormaValue }
  | FormaValue[]
export type FormaValueIsSchema = { $schema: true }
export type FormaTypeRef = Function | readonly [FormaTypeRef]
export type FormaReturnTypeRef<T extends FormaTypeRef = any> = (type?: void) => T
export type FormaValidationRule =
  | FormaConditionalValidationRule
  | FormaCustomValidationRule
  | FormaIsDefinedValidationRule
  | FormaNestedValidationRule
  | FormaPromiseValidationRule
  | FormaUnknownValidationRule
  | FormaWhitelistValidationRule

export interface FormaOptions {
  readonly type?: FormaTypeRef
  readonly name?: string
  readonly default?: FormaValue
  readonly groups?: readonly Suggest<'*' | 'C' | 'U'>[]
  readonly always?: boolean
}

interface DecoratorOptions {
  readonly targetConstructor: Function
  readonly propertyName: string | symbol
  readonly options: FormaOptions
}

interface MetadataArgs {
  readonly targetConstructor: Function
  readonly propertyName: string | symbol
  readonly options: FormaOptions
}

export interface FormaSchema<T extends FormaValueIsSchema = any> {
  readonly type: string
  readonly name: string
  readonly fields: {
    readonly [K in keyof T]: T[K] extends FormaValueIsSchema
      ? FormaSchema<T[K]>
      : T[K] extends FormaValue
      ? FormaField<T[K]>
      : never
  }
}

export interface FormaField<T extends FormaValue = any> {
  readonly type: string
  readonly name: string
  readonly default: T | undefined
  readonly validationRules: readonly FormaValidationRule[]
}

export interface FormaConditionalValidationRule {
  readonly type: '?'
  readonly name: string
}

export interface FormaCustomValidationRule {
  readonly type: 'custom'
  readonly name: string
  readonly constraints: readonly FormaValue[] | undefined
  readonly defaultMessages: readonly FormaMessage[]
  readonly customMessages: readonly FormaMessage[] | undefined
  readonly groups: readonly string[] | undefined
  readonly always: boolean | undefined
  readonly each: true | undefined
  readonly context: FormaValue | undefined
}

export interface FormaIsDefinedValidationRule {
  readonly type: 'isDefined'
  readonly defaultMessages: readonly FormaMessage[]
  readonly customMessages: readonly FormaMessage[] | undefined
  readonly groups: readonly string[] | undefined
  readonly always: boolean | undefined
  readonly each: true | undefined
  readonly context: FormaValue | undefined
}

export interface FormaNestedValidationRule {
  readonly type: 'nested'
  readonly defaultMessages: readonly FormaMessage[]
  readonly customMessages: readonly FormaMessage[] | undefined
  readonly groups: readonly string[] | undefined
  readonly always: boolean | undefined
  readonly each: true | undefined
  readonly context: FormaValue | undefined
}

export interface FormaPromiseValidationRule {
  readonly type: 'promise'
}

export interface FormaUnknownValidationRule {
  readonly type: 'unknown'
  readonly typeName: string
}

export interface FormaWhitelistValidationRule {
  readonly type: 'whitelist'
}

export interface FormaMessage {
  readonly message: string
  readonly messageForEach: string
  readonly givenValue?: unknown
  readonly givenConstraints?: unknown[]
}

export function Forma(options?: FormaOptions): PropertyDecorator

export function Forma<T extends FormaTypeRef>(
  type: FormaReturnTypeRef<T>
): PropertyDecorator

export function Forma<T extends FormaTypeRef>(
  optionsOrType: FormaOptions | FormaReturnTypeRef<T> = {}
): PropertyDecorator {
  if (typeof optionsOrType === 'function') {
    optionsOrType = { type: optionsOrType() }
  }

  const options = optionsOrType

  return (target, propertyKey) => {
    registerDecorator({
      targetConstructor: target.constructor,
      propertyName: propertyKey,
      options,
    })
  }
}

export function getMetadataFor(
  targetConstructor: Function,
  groups: string | readonly string[] | null = '*',
  strictGroups: boolean = true,
  alwaysDefault: boolean = false,
): Record<string | symbol, Metadata> {
  return getMetadataStorage().getMetadataFor(
    targetConstructor,
    groups,
    strictGroups,
    alwaysDefault,
  )
}

function getGlobal(): typeof globalThis {
  if (globalThis !== undefined) {
    return globalThis
  }

  if (global !== undefined) {
    return global
  }

  // @ts-expect-error TS2304
  //   - TS2345: `Cannot find name 'window'.`
  if (window !== undefined) {
    // @ts-expect-error TS2304
    //   - TS2345: `Cannot find name 'window'.`
    return window
  }

  // @ts-expect-error TS2304
  //   - TS2345: `Cannot find name 'window'.`
  if (self !== undefined) {
    // @ts-expect-error TS2304
    //   - TS2345: `Cannot find name 'window'.`
    return self
  }

  throw new Error('Variable representing global scope does not exist.')
}

function getMetadataStorage(): MetadataStorage {
  const global = getGlobal()

  if (!global.formaMetadataStorage) {
    global.formaMetadataStorage = new MetadataStorage()
  }

  return global.formaMetadataStorage
}

function registerDecorator(options: DecoratorOptions): void {
  getMetadataStorage().addMetadata(new Metadata({
    targetConstructor: options.targetConstructor,
    propertyName: options.propertyName,
    options: options.options,
  }))
}

class MetadataStorage {
  private readonly metadata: Map<Function, Metadata[]> = new Map()

  addMetadata(metadata: Metadata): void {
    const existingMetadata = this.metadata.get(metadata.targetConstructor)

    if (existingMetadata) {
      existingMetadata.push(metadata)
    } else {
      this.metadata.set(metadata.targetConstructor, [metadata])
    }
  }

  getMetadataFor(
    targetConstructor: Function,
    groups: string | readonly string[] | null,
    strictGroups: boolean,
    alwaysDefault: boolean,
  ): ReturnType<typeof getMetadataFor> {
    if (typeof groups === 'string') {
      groups = [groups]
    }

    const originalMetadata = this.metadata.get(targetConstructor) || []
    const inheritedMetadata: Metadata[] = []

    for (const [key, value] of this.metadata.entries()) {
      if (key !== targetConstructor && targetConstructor.prototype instanceof key) {
        inheritedMetadata.push(...value)
      }
    }

    // @because We want to filter out duplicate metadata because we prefer
    //   original metadata over inherited metadata.
    const uniqueInheritedMetadata = inheritedMetadata.filter(inherited => {
      return !originalMetadata.find(original => {
        return original.propertyName === inherited.propertyName
      })
    })

    const metadata: ReturnType<MetadataStorage['getMetadataFor']> = {}

    for (const md of originalMetadata.concat(uniqueInheritedMetadata)) {
      if (md.always) {
        metadata[md.propertyName] = md
        continue
      }

      if (groups?.length) {
        if (md.groups.length === 1) { // contains * only
          if (!strictGroups) {
            metadata[md.propertyName] = md
          }

          continue
        }

        if (groups.findIndex(group => md.groups.includes(group)) !== -1) {
          metadata[md.propertyName] = md
        }

        continue
      }

      if (md.always === undefined && alwaysDefault) {
        metadata[md.propertyName] = md
      }
    }

    return metadata
  }
}

export class Metadata {
  readonly targetConstructor: Function
  readonly propertyName: string | symbol
  readonly name: string
  readonly default: FormaValue | undefined
  readonly groups: readonly string[]
  readonly always: boolean | undefined
  protected _type?: Type
  protected _validationRules?: readonly FormaValidationRule[]

  get type(): Type {
    if (!this._type) {
      this._type = new Type(Reflect.getMetadata(
        'design:type',
        this.targetConstructor,
        this.propertyName,
      ) as Function || Object)
    }

    return this._type
  }

  get validationRules(): readonly FormaValidationRule[] {
    if (!this._validationRules) {
      const validationRules: FormaValidationRule[] = []
      const cvValidationMetadata = getClassValidatorMetadataStorage()
        .getTargetValidationMetadatas(
          this.targetConstructor,
          // @ts-expect-error TS2345
          //   - TS2345: `Argument of type 'undefined' is not assignable to parameter of type 'string'.`
          undefined,
          true,
          false,
        )
        .filter(cvvm => this.propertyName === cvvm.propertyName)

      for (const cvvm of cvValidationMetadata) {
        const defaultMessages: FormaMessage[] = []
        const customMessages: FormaMessage[] = []
        const cvDefaultMessage = (
          // typed badly, can also be `undefined`
          cvvm.constraintCls?.prototype as ClassValidatorConstraintInterface
        )?.defaultMessage || (() => '(no default message)')
        const cvDefaultMessageArgs: ClassValidatorValidationArguments = {
          value: undefined,
          constraints: cvvm.constraints, // typed badly, can also be `undefined`
          targetName: (cvvm.target as Function).name,
          object: {},
          property: cvvm.propertyName,
        }

        if (!cvvm.name) {
          switch (cvvm.type) {
            case ClassValidatorValidationTypes.CONDITIONAL_VALIDATION:
              const evaluate: (
                object: object,
                value: unknown,
              ) => boolean = cvvm.constraints![0]!
              cvvm.name = !evaluate({}, true) && !evaluate({
                [cvvm.propertyName]: null
              }, true) && evaluate({
                [cvvm.propertyName]: false
              }, false) && evaluate({
                [cvvm.propertyName]: 0
              }, false) && evaluate({
                [cvvm.propertyName]: NaN
              }, false) && evaluate({
                [cvvm.propertyName]: ''
              }, false) ? 'isOptional' : 'if'
              break

            case ClassValidatorValidationTypes.NESTED_VALIDATION:
              cvvm.name = '(nested)'
              break

            case ClassValidatorValidationTypes.PROMISE_VALIDATION:
              cvvm.name = '(promise)'
              break

            case ClassValidatorValidationTypes.WHITELIST:
              cvvm.name = '(allow)'
              break

            default:
              cvvm.name = '(unknown)'
              break
          }
        }

        // @because (`if`) The `message` is typed badly, can also be `undefined`.
        if (cvvm.message) {
          if (typeof cvvm.message === 'string') {
            customMessages.push({
              message: cvvm.message,
              messageForEach: 'each value in ' + cvvm.message,
            })
          } else {
            for (let i = 0, prevMessage: string | null = null; i <= 100; i++) {
              if (i === 100) {
                const ref = cvDefaultMessageArgs.targetName
                  + '.' + cvDefaultMessageArgs.property
                  + '@' + cvvm.name
                  + '(' + (cvvm.constraints ?? '') + ')'
                throw new Error(
                  `Check your custom messages of '${ref}' because infinity loop was detected.`
                )
              }

              const si = Symbol(i)
              // @ts-expect-error TS2554
              //   - TS2554: `Expected 1 arguments, but got 3.`
              const message = cvvm.message(cvDefaultMessageArgs, si, false)
              // @ts-expect-error TS2554
              //   - TS2554: `Expected 1 arguments, but got 3.`
              const messageForEach = cvvm.message(cvDefaultMessageArgs, si, true)

              if (!message || message === prevMessage) {
                break
              }

              customMessages.push({
                message,
                messageForEach,
                givenConstraints: cvDefaultMessageArgs.constraints,
              })
              prevMessage = message
            }
          }
        }

        switch (cvvm.name) {
          case classValidators['IS_INSTANCE']:
            defaultMessages.push({
              message: cvDefaultMessage({
                ...cvDefaultMessageArgs,
                constraints: [{ name: '$constraint1' }],
              }),
              messageForEach: 'each value in ' + cvDefaultMessage({
                ...cvDefaultMessageArgs,
                constraints: [{ name: '$constraint1' }],
              }),
              givenConstraints: [{ name: '$constraint1' }],
            })
            break

          case classValidators['IS_LENGTH']:
            defaultMessages.push({
              message: cvDefaultMessage({
                ...cvDefaultMessageArgs,
                constraints: [1],
              }),
              messageForEach: 'each value in ' + cvDefaultMessage({
                ...cvDefaultMessageArgs,
                constraints: [1],
              }),
              givenConstraints: [1],
            }, {
              message: cvDefaultMessage({
                ...cvDefaultMessageArgs,
                value: '42',
                constraints: [null, 1],
              }),
              messageForEach: 'each value in ' + cvDefaultMessage({
                ...cvDefaultMessageArgs,
                value: '42',
                constraints: [null, 1],
              }),
              givenValue: '42',
              givenConstraints: [null, 1],
            }, {
              message: cvDefaultMessage({
                ...cvDefaultMessageArgs,
                constraints: [],
              }),
              messageForEach: 'each value in ' + cvDefaultMessage({
                ...cvDefaultMessageArgs,
                constraints: [],
              }),
              givenConstraints: [],
            })
            break

          case classValidators['MAX_DATE']:
            defaultMessages.push({
              message: 'maximal allowed date for $property is $constraint1',
              messageForEach: 'maximal allowed date for each value in $property is $constraint1',
              givenConstraints: cvDefaultMessageArgs.constraints,
            })
            break

          case classValidators['MIN_DATE']:
            defaultMessages.push({
              message: 'minimal allowed date for $property is $constraint1',
              messageForEach: 'minimal allowed date for each value in $property is $constraint1',
              givenConstraints: cvDefaultMessageArgs.constraints,
            })
            break

          case '(nested)':
            defaultMessages.push({
              message: 'nested property $property must be either object or array',
              messageForEach: 'each value in nested property $property must be either object or array',
            })
            break

          case 'if':
          case 'isOptional':
          case '(allow)':
          case '(promise)':
          case '(unknown)':
            // There is no default message.
            break

          default:
            defaultMessages.push({
              message: cvDefaultMessage(cvDefaultMessageArgs),
              messageForEach: 'each value in ' + cvDefaultMessage(cvDefaultMessageArgs),
              givenConstraints: cvDefaultMessageArgs.constraints,
            })
            break
        }

        switch (cvvm.type) {
          case ClassValidatorValidationTypes.CONDITIONAL_VALIDATION:
            validationRules.push({ type: '?', name: cvvm.name })
            break

          case ClassValidatorValidationTypes.CUSTOM_VALIDATION:
            validationRules.push({
              type: 'custom',
              name: cvvm.name,
              constraints: cvvm.constraints?.length ? cvvm.constraints : undefined,
              defaultMessages,
              customMessages: customMessages.length ? customMessages : undefined,
              // typed badly, can also be `undefined`
              groups: cvvm.groups?.length ? cvvm.groups : undefined,
              always: cvvm.always,
              each: cvvm.each || undefined,
              context: cvvm.context,
            })
            break

          case ClassValidatorValidationTypes.IS_DEFINED:
            validationRules.push({
              type: 'isDefined',
              defaultMessages,
              customMessages: customMessages.length ? customMessages : undefined,
              groups: cvvm.groups?.length ? cvvm.groups : undefined,
              always: cvvm.always,
              each: cvvm.each || undefined,
              context: cvvm.context,
            })
            break

          case ClassValidatorValidationTypes.NESTED_VALIDATION:
            validationRules.push({
              type: 'nested',
              defaultMessages,
              customMessages: customMessages.length ? customMessages : undefined,
              groups: cvvm.groups?.length ? cvvm.groups : undefined,
              always: cvvm.always,
              each: cvvm.each || undefined,
              context: cvvm.context,
            })
            break

          case ClassValidatorValidationTypes.PROMISE_VALIDATION:
            validationRules.push({ type: 'promise' })
            break

          case ClassValidatorValidationTypes.WHITELIST:
            validationRules.push({ type: 'whitelist' })
            break

          default:
            validationRules.push({ type: 'unknown', typeName: cvvm.type })
            break
        }
      }

      this._validationRules = validationRules
    }

    return this._validationRules
  }

  constructor ({ targetConstructor, propertyName, options }: MetadataArgs) {
    if (options.groups?.includes('*')) {
      throw new Error('You cannot use * as group name.')
    }

    if (options.type) {
      this._type = new Type(options.type)
    } else {
      switch (options.default !== null ? typeof options.default : 'undefined') {
        case 'boolean':
          this._type = new Type(Boolean)
          break

        case 'bigint':
          this._type = new Type(BigInt)
          break

        case 'function':
          this._type = new Type(Function)
          break

        case 'number':
          this._type = new Type(Number)
          break

        case 'object':
          this._type = new Type(Object)
          break

        case 'string':
          this._type = new Type(String)
          break

        case 'symbol':
          this._type = new Type(Symbol)
          break

        case 'undefined':
          break

        default:
          throw new Error(`Missing logic for '${typeof options.default}'.`)
      }
    }

    this.targetConstructor = targetConstructor
    this.propertyName = propertyName
    this.name = options.name || propertyName.toString()
    this.default = options.default
    this.groups = options.groups ? ['*', ...options.groups] : ['*']
    this.always = options.always
  }

  isNestable(): boolean {
    return this.type.isClassRef()
  }

  getNestedClassRef(): Newable<FormaTypeObject> {
    return this.type.getAsClassRef()
  }

  getValidationJsonRulesFor(
    groups: ReadonlyArrayOrSingle<string> | null = [],
    strictGroups: boolean = false,
    alwaysDefault: boolean = false,
  ): FormaJsonValidationRule[] {
    if (groups === null) {
      groups = []
    }

    return this.validationMetadata.filter(vmd => {
      if (vmd.always || !groups!.length) {
        return true
      }

      for (const group of groups!) {
        if (vmd.groups.includes(group)) {
          return true
        }
      }

      return false
    })
  }
}

export class Type {
  constructor (x: FormaTypeRef) {
    if (x === Function) {
      throw new TypeError()
    }

    if (x === Object) {
      throw new TypeError()
    }

    if (x === Symbol) {
      throw new TypeError()
    }
  }

  isClassRef(): boolean {
    return false
  }

  getAsClassRef(): Newable<FormaTypeObject> {
    return null!
  }

  toString(): string {
    return 'TO_STRING'
  }
}
