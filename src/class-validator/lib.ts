import { buildMessage } from 'class-validator';
import { getMetadataStorage } from 'class-validator';
import { ValidateBy } from 'class-validator';
import type { ValidationOptions } from 'class-validator';
import { ValidationTypes } from 'class-validator';
import { ValidationMetadata } from 'class-validator/cjs/metadata/ValidationMetadata.js'
import type { ValidationMetadataArgs } from 'class-validator/cjs/metadata/ValidationMetadataArgs.js'
import { Transform } from 'class-transformer';

export type CustomClassValidatorNames =
  | 'isNotRequired'
  | 'isNotRequiredWithTrim'
  | 'isRequired'
  | 'isRequiredIfPresent'
  | 'isRequiredIfPresentWithTrim'
  | 'isTrimmed'

export function IsNotRequired(
  validationOptions?: ValidationOptions,
): PropertyDecorator

export function IsNotRequired(
  trim: boolean,
  validationOptions?: ValidationOptions,
): PropertyDecorator

export function IsNotRequired(
  trim: boolean,
  uselessToNull: boolean,
  validationOptions?: ValidationOptions,
): PropertyDecorator

export function IsNotRequired(
  trim: boolean | ValidationOptions = true,
  uselessToNull: boolean | ValidationOptions = true,
  validationOptions: ValidationOptions = {},
): PropertyDecorator {
  if (typeof trim === 'object') {
    validationOptions = trim
    trim = true
    uselessToNull = true
  } else if (typeof uselessToNull === 'object') {
    validationOptions = uselessToNull
    uselessToNull = true
  }

  return (target, propertyKey) => {
    const args: ValidationMetadataArgs = {
      type: ValidationTypes.CONDITIONAL_VALIDATION,
      name: trim ? 'isNotRequiredWithTrim' : 'isNotRequired',
      target: target.constructor,
      propertyName: propertyKey as string,
      constraints: [
        (_: Object, value: unknown): boolean => isUseful(value, trim as boolean),
      ],
      validationOptions,
    }

    getMetadataStorage().addValidationMetadata(new ValidationMetadata(args))

    Transform(({ value }) => {
      value = trim && typeof value === 'string' ? value.trim() : value
      return isUseful(value, false) ? value : (uselessToNull ? null : value)
    }, {
      groups: validationOptions.groups || [],
    })(target, propertyKey)
  }
}

export function IsRequired(
  validationOptions?: ValidationOptions,
): PropertyDecorator

export function IsRequired(
  trim: boolean,
  validationOptions?: ValidationOptions,
): PropertyDecorator

export function IsRequired(
  trim: boolean | ValidationOptions = true,
  validationOptions?: ValidationOptions,
): PropertyDecorator {
  if (typeof trim === 'object') {
    validationOptions = trim
    trim = true
  }

  return ValidateBy({
    name: 'isRequired',
    constraints: [trim],
    validator: {
      validate: (value) => isUseful(value, trim as boolean),
      defaultMessage: buildMessage(
        eachPrefix => eachPrefix + '$property is required',
        validationOptions,
      ),
    },
  }, validationOptions)
}

export function IsRequiredIfPresent(
  validationOptions?: ValidationOptions,
): PropertyDecorator

export function IsRequiredIfPresent(
  trim: boolean,
  validationOptions?: ValidationOptions,
): PropertyDecorator

export function IsRequiredIfPresent(
  trim: boolean | ValidationOptions = true,
  validationOptions: ValidationOptions = {},
): PropertyDecorator {
  if (typeof trim === 'object') {
    validationOptions = trim
    trim = true
  }

  return (target, propertyKey) => {
    const args: ValidationMetadataArgs = {
      type: ValidationTypes.CONDITIONAL_VALIDATION,
      name: trim ? 'isRequiredIfPresentWithTrim' : 'isRequiredIfPresent',
      target: target.constructor,
      propertyName: propertyKey as string,
      constraints: [
        (_: Object, value: unknown): boolean => {
          return value !== undefined
        },
      ],
      validationOptions,
    }

    getMetadataStorage().addValidationMetadata(new ValidationMetadata(args))
    IsRequired(trim as boolean, validationOptions)(target, propertyKey)
  }
}

export function IsTrimmed(
  validationOptions?: ValidationOptions,
): PropertyDecorator {
  return ValidateBy({
    name: 'isTrimmed',
    validator: {
      validate: value => isTrimmed(value),
      defaultMessage: buildMessage(
        eachPrefix => eachPrefix + '$property must be trimmed',
        validationOptions,
      ),
    },
  }, validationOptions)
}

export function isUseful(value: unknown, trim: boolean = true): boolean {
  if (value === undefined || value === null) {
    return false
  }

  const type = typeof value

  if (type === 'string') {
    return (trim ? (value as string).trim() : value) !== ''
  }

  if (type === 'object') {
    for (const _ in value) {
      return true
    }

    return value instanceof RegExp || value instanceof Date
  }

  return true
}

export function isTrimmed(value: unknown): boolean {
  return typeof value === 'string'
    && !value.startsWith(' ')
    && !value.endsWith(' ')
}
